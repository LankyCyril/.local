.local
======

A collection of:

1. Small scripts written by me.
2. Tools from other developers, imported as submodules:
    1. written in interpreted languages and added to $PATH,
    2. written in compiled languages, with a script to make all.
